pub mod header;
pub mod record;
pub mod reader;

use std::collections::HashMap;

pub use reader::Reader;
pub use record::{Record, RecordData};
pub use header::{Header, DirectoryEntry, Tag};
use serde::{Serialize, Deserialize};

static MAGIC_NUMBER: &[u8] = b"ABIF";


#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    InvalidData(String),
}

impl From<std::io::Error> for Error {
    fn from(source: std::io::Error) -> Self {
        Self::IO(source)
    }
}


pub struct Abif {
    pub record_map: HashMap<Tag, RecordData>,
}

impl Abif
{
    pub fn read<R>(reader: R) -> Result<Abif, Error>
    where R: std::io::Read + std::io::Seek,
    {
        let mut reader = Reader::new(reader);
        let header = reader.read_header()?;
        let directory = reader.read_directory(&header)?;
        let records = reader.read_records(&directory)?;

        let mut record_map = HashMap::new();
        for record in records {
            let (tag, data) = (record.tag, record.data);
            record_map.insert(tag, data);
        }

        Ok(Abif { record_map })
    }

    pub fn get(&self, name: &str, number: i32) -> Option<&RecordData> {
        self.record_map.get(&Tag::new(name.to_string(), number))
    }

    pub fn get_text(&self, name: &str, number: i32) -> Option<String> {
        match self.get(name, number) {
            Some(RecordData::Text(text)) => Some(text.clone()),
            _ => None,
        }
    }

    pub fn get_i32(&self, name: &str, number: i32) -> Option<i32> {
        match self.get(name, number) {
            Some(RecordData::Integers(arr)) => {
                if arr.len() == 1 {
                    Some(arr[0])
                } else {
                    None // only expecting one element
                }
            },
            _ => None,
        }
    }

    pub fn get_f32(&self, name: &str, number: i32) -> Option<f32> {
        match self.get(name, number) {
            Some(RecordData::Floats(arr)) => {
                if arr.len() == 1 {
                    Some(arr[0])
                } else {
                    None // only expecting one element
                }
            },
            _ => None,
        }
    }

    pub fn get_u8(&self, name: &str, number: i32) -> Option<u8> {
        match self.get(name, number) {
            Some(RecordData::Bytes(arr)) => {
                if arr.len() == 1 {
                    Some(arr[0])
                } else {
                    None // only expecting one element
                }
            },
            _ => None,
        }
    }

    pub fn get_i32_vec(&self, name: &str, number: i32) -> Option<Vec<i32>> {
        match self.get(name, number) {
            Some(RecordData::Integers(data)) => Some(data.clone()),
            _ => None
        }
    }

    pub fn get_u8_vec(&self, name: &str, number: i32) -> Option<Vec<u8>> {
        match self.get(name, number) {
            Some(RecordData::Bytes(data)) => Some(data.clone()),
            _ => None
        }
    }

    pub fn metadata(&self) -> Metadata {
        Metadata {
            analysis_protocol: self.analysis_protocol(),
            software_versions: self.software_versions(),
            peak_spacing: self.peak_spacing(),
            separation_medium: self.separation_medium(),
            run: self.run(),
            data_collection: self.data_collection(),
            results_group: self.results_group(),
            plate: self.plate(),
            instrument: self.instrument(),
            dyes: self.dyes(),
            container: self.container(),
            analysis: self.analysis(),

            scans: self.scans(),
            sample_name: self.sample_name(),
            dye_signal_strength: self.dye_signal_strength(),
            complemented: self.complemented(),
            pixel_bin_size: self.pixel_bin_size(),
            baseline_noise: self.baseline_noise(),
            total_capillaries: self.total_capillaries(),
            averaged_columns: self.averaged_columns(),
        }
    }

    pub fn trace_data(&self) -> TraceData {
        TraceData {
            peak_locations: self.peak_locations(),
            quality_values: self.quality_values(),
            sequences: self.sequence_values(),
            raw_colors: self.raw_color_data(),
            analyzed_colors: self.analyzed_color_data(),
            measurements: self.measurement_data(),
        }
    }

    pub fn base_order(&self) -> Option<String> {
        match self.get("FWO_", 1) {
            Some(RecordData::Bytes(chars)) => Some(chars_to_utf8(chars)),
            _ => None
        }
    }

    pub fn g_analyzed(&self) -> Option<Vec<i32>> {
        self.base_analyzed("G")
    }

    pub fn a_analyzed(&self) -> Option<Vec<i32>> {
        self.base_analyzed("A")
    }

    pub fn t_analyzed(&self) -> Option<Vec<i32>> {
        self.base_analyzed("T")
    }

    pub fn c_analyzed(&self) -> Option<Vec<i32>> {
        self.base_analyzed("C")
    }

    pub fn g_raw(&self) -> Option<Vec<i32>> {
        self.base_raw("G")
    }

    pub fn a_raw(&self) -> Option<Vec<i32>> {
        self.base_raw("A")
    }

    pub fn t_raw(&self) -> Option<Vec<i32>> {
        self.base_raw("T")
    }

    pub fn c_raw(&self) -> Option<Vec<i32>> {
        self.base_raw("C")
    }

    pub fn analysis_protocol(&self) -> AnalysisProtocol {
        let settings_version = self.get_text("APrV", 1);
        let xml_schema_version = self.get_text("APXV", 1);
        let xml = match self.get("APrX", 1) {
            Some(RecordData::Bytes(chars)) => Some(chars_to_utf8(chars)),
            _ => None,
        };
        let settings_name = match self.get_text("APrN", 1) {
            Some(name) => Some(name),
            _ => self.get_text("APFN", 2)
        };

        AnalysisProtocol {
            settings_name,
            settings_version,
            xml_schema_version,
            xml,
        }
    }

    pub fn software_versions(&self) -> SoftwareVersions {
        let data_collection = self.get_text("SVER", 1);
        let sizecaller = self.get_text("SVER", 2);
        let firmware = self.get_text("SVER", 3);

        SoftwareVersions {
            data_collection,
            sizecaller,
            firmware,
        }
    }

    pub fn scans(&self) -> Option<i32> {
        // legacy fallback
        match self.get_i32("SCAN", 1) {
            Some(scans) => Some(scans),
            None => self.get_i32("Scan", 1),
        }
    }

    pub fn peak_spacing(&self) -> PeakSpacing {
        let avg_used_in_last_analysis = self.get_f32("SPAC", 1);
        let basecaller_name = self.get_text("SPAC", 2);
        let avg_last_calculated = self.get_f32("SPAC", 3);

        PeakSpacing {
            avg_used_in_last_analysis,
            avg_last_calculated,
            basecaller_name,
        }
    }

    pub fn sample_name(&self) -> Option<String> {
        self.get_text("SMPL", 1)
    }

    pub fn separation_medium(&self) -> SeparationMedium {
        let lot_number = self.get_text("SMLt", 1);
        let lot_expiry = self.get_text("SMED", 1);

        SeparationMedium {
            lot_number,
            lot_expiry,
        }
    }

    pub fn dye_signal_strength(&self) -> Option<(i32, i32, i32, i32)> {
        match self.get("S/N%", 1) {
            Some(RecordData::Integers(arr)) => {
                if arr.len() == 4 {
                    Some((arr[0], arr[1], arr[2], arr[3]))
                } else {
                    None
                }
            },
            _ => None,
        }
    }

    pub fn run(&self) -> Run {
        let name = self.get_text("RunN", 1);
        let temperature = self.get_i32("Tmpr", 1);
        let start_date = self.get_text("RUND", 1);
        let start_time = self.get_text("RUNT", 1);
        let end_date = self.get_text("RUND", 2);
        let end_time = self.get_text("RUNT", 2);
        let protocol_name = self.get_text("RPrN", 1);
        let protocol_version = self.get_text("RPrV", 1);

        let module_version = self.get_text("RMdV", 1);
        let module_schema_version = self.get_text("RMXV", 1);
        let module_xml = match self.get("RMdX", 1) {
            Some(RecordData::Bytes(chars)) => Some(chars_to_utf8(chars)),
            _ => None,
        };
        let module_name = match self.get_text("RMdN", 1) {
            Some(name) => Some(name),
            None => self.get_text("MODF", 1),
        };

        Run {
            name,
            temperature,
            start_date,
            start_time,
            end_date,
            end_time,

            protocol_name,
            protocol_version,

            module_name,
            module_version,
            module_schema_version,
            module_xml,
        }
    }

    pub fn data_collection(&self) -> DataCollection {
        let start_date = self.get_text("RUND", 3);
        let start_time = self.get_text("RUNT", 3);
        let end_date = self.get_text("RUND", 4);
        let end_time = self.get_text("RUNT", 4);

        DataCollection {
            start_date,
            start_time,
            end_date,
            end_time,
        }
    }

    pub fn complemented(&self) -> Option<i32> {
        self.get_i32("RevC", 1)
    }

    pub fn results_group(&self) -> ResultsGroup {
        let name = self.get_text("RGNm", 1);
        let owner = self.get_text("RGOw", 1);
        let comment = self.get_text("RGCm", 1);

        ResultsGroup {
            name,
            owner,
            comment,
        }
    }

    pub fn plate(&self) -> Plate {
        let r#type = self.get_text("PTYP", 1);
        let size = self.get_i32("PSZE", 1);

        Plate {
            r#type,
            size,
        }
    }

    pub fn instrument(&self) -> Instrument {
        Instrument {
            class: self.get_text("HCFG", 1),
            family: self.get_text("HCFG", 2),
            name: self.get_text("HCFG", 3),
            parameters: self.get_text("HCFG", 4),
        }
    }

    pub fn dyes(&self) -> Dyes {
        Dyes {
            total: self.get_i32("Dye#", 1),
            name: self.get_text("DySN", 1),
            dye1: self.dye_details(1),
            dye2: self.dye_details(2),
            dye3: self.dye_details(3),
            dye4: self.dye_details(4),
        }
    }

    pub fn dye_details(&self, number: i32) -> Option<Dye> {
        if let Some(name) = self.get_text("DyeN", number) {
            Some(Dye {
                name: Some(name),
                wavelength: self.get_i32("DyeW", number),
            })
        } else {
            None
        }
    }

    pub fn container(&self) -> Container {
        Container {
            id: self.get_text("CTID", 1),
            name: self.get_text("CTNM", 1),
            owner: self.get_text("CTOw", 1),
        }
    }

    pub fn analysis(&self) -> Analysis {
        Analysis {
            return_code: self.get_i32("ARTN", 1),
            adaptive_processing: self.get_i32("ASPF", 1),
            starting_scan_first: self.get_i32("ASPt", 1),
            starting_scan_last: self.get_i32("ASPt", 2),
            ending_scan_first: self.get_i32("AEPt", 1),
            ending_scan_last: self.get_i32("AEPt", 2),
            ref_scan_first: self.get_i32("B1Pt", 1),
            ref_scan_last: self.get_i32("B1Pt", 2),
            basecaller_completed: self.get_text("BCTS", 1),
        }
    }

    pub fn pixel_bin_size(&self) -> Option<i32> {
        self.get_i32("PXLB", 1)
    }

    pub fn baseline_noise(&self) -> Option<(f32, f32, f32, f32)> {
        match self.get("NOIS", 1) {
            Some(RecordData::Floats(arr)) => {
                if arr.len() == 4 {
                    Some((arr[0], arr[1], arr[2], arr[3]))
                } else {
                    None
                }
            },
            _ => None,
        }
    }

    pub fn total_capillaries(&self) -> Option<i32> {
        self.get_i32("NLNE", 1)
    }

    pub fn averaged_columns(&self) -> Option<i32> {
        self.get_i32("NAVG", 1)
    }

    fn base_analyzed(&self, base: &str) -> Option<Vec<i32>> {
        if let Some(channel) = self.base_order_channel(base) {
            self.get_i32_vec("DATA", channel)
        } else {
            None
        }
    }

    fn base_raw(&self, base: &str) -> Option<Vec<i32>> {
        if let Some(channel) = self.base_raw_order_channel(base) {
            self.get_i32_vec("DATA", channel)
        } else {
            None
        }
    }

    fn base_order_channel(&self, base: &str) -> Option<i32> {
        if let Some(order) = self.base_order() {
            if let Some(idx) = order.find(base) {
                return Some(idx as i32 + 9);
            }
        }
        None
    }

    fn base_raw_order_channel(&self, base: &str) -> Option<i32> {
        if let Some(order) = self.base_order() {
            if let Some(idx) = order.find(base) {
                return Some(idx as i32 + 1);
            }
        }
        None
    }


    pub fn peak_locations(&self) -> PeakLocations {
        PeakLocations {
            edited_by_user: self.get_i32_vec("PLOC", 1),
            called_by_basecaller: self.get_i32_vec("PLOC", 2),
        }
    }

    pub fn quality_values(&self) -> QualityValues {
        QualityValues {
            edited_by_user: self.get_u8_vec("PCON", 1),
            called_by_basecaller: self.get_u8_vec("PCON", 2),
        }
    }

    pub fn sequence_values(&self) -> SequenceValues {
        SequenceValues {
            edited_by_user: self.get_u8_vec("PBAS", 1),
            called_by_basecaller: self.get_u8_vec("PBAS", 2),
        }
    }

    pub fn analyzed_color_data(&self) -> AnalyzedColorData {
        AnalyzedColorData {
            channel_1: self.get_i32_vec("DATA", 9),
            channel_2: self.get_i32_vec("DATA", 10),
            channel_3: self.get_i32_vec("DATA", 11),
            channel_4: self.get_i32_vec("DATA", 12),
        }
    }

    pub fn measurement_data(&self) -> MeasurementData {
        MeasurementData {
            voltage: self.get_i32_vec("DATA", 5),
            current: self.get_i32_vec("DATA", 6),
            power: self.get_i32_vec("DATA", 7),
            temperature: self.get_i32_vec("DATA", 8),
        }
    }

    pub fn raw_color_data(&self) -> RawColorData {
        RawColorData {
            channel_1: self.get_i32_vec("DATA", 1),
            channel_2: self.get_i32_vec("DATA", 2),
            channel_3: self.get_i32_vec("DATA", 3),
            channel_4: self.get_i32_vec("DATA", 4),
        }
    }


    // Set Trim region
    pub fn trim_region(&self) -> Option<i32> {
        self.get_i32("phTR", 1)
    }

    // Maximum Quality Value
    pub fn max_quality_value(&self) -> Option<i32> {
        self.get_i32("phQL", 1)
    }

    // Dye ("big", "d-rhod", "unknown"), based on mob file information
    pub fn dye(&self) -> Option<String> {
        self.get_text("phDY", 1)
    }

    // Chemistry type ("term", "prim", "unknown"), based on DYE_1 information
    pub fn chemistry_type(&self) -> Option<String> {
        self.get_text("phCH", 1)
    }

    // Trace peak aria ratio
    pub fn trace_peak_aria_ratio(&self) -> Option<f32> {
        self.get_f32("phAR", 1)
    }

    // Well ID
    // Autosampler position (A1-H1) (CapEP only)
    pub fn well_id(&self) -> Option<String> {
        self.get_text("TUBE", 1)
    }

    // Rescaling divisor for color data
    // Rescaling Divisor for reducing the dynamic range of the color data
    pub fn rescaling_divisor(&self) -> Option<f32> {
        self.get_f32("Scal", 1)
    }

    // Scanning rate
    // Scanning Rate. Milliseconds per frame.
    pub fn scanning_rate(&self) -> Option<String> {
        self.get_text("Rate", 1)
    }

    // Mobility file (orig)
    // Sequencing Analysis Mobility file name chosen in collection
    pub fn mobility_file_name(&self) -> Option<String> {
        self.get_text("PDMF", 1)
    }

    // Model number
    // Model number. 4 characters
    pub fn model_number(&self) -> Option<Vec<u8>> {
        self.get_u8_vec("MODL", 1)
    }

    // Instrument name and serial number
    // Machine Name. (instrument serial number can be found in HCFG-4)
    pub fn instrumment_name(&self) -> Option<String> {
        self.get_text("MCHN", 1)
    }

    // Laser Power setting (micro Watts)
    pub fn laser_power_setting(&self) -> Option<i32> {
        self.get_i32("LsrP", 1)
    }

    // Length To Detector (capillary length)
    pub fn length_to_detector(&self) -> Option<i32> {
        self.get_i32("LNTD", 1)
    }

    // Sample Tracking ID
    pub fn sample_tracking_id(&self) -> Option<String> {
        self.get_text("LIMS", 1)
    }

    // Sample's lane or capillary number
    pub fn sample_lane(&self) -> Option<i32> {
        self.get_i32("LANE", 1)
    }

    // Injection Voltage setting in Volts
    pub fn injection_voltage(&self) -> Option<i32> {
        self.get_i32("InVt", 1)
    }

    // Injection time setting in Seconds
    pub fn injection_time(&self) -> Option<i32> {
        self.get_i32("InSc", 1)
    }

    // Gel type description
    pub fn gel_type(&self) -> Option<String> {
        self.get_text("GTyp", 1)
    }

    // Electrophoresis voltage setting (volts)
    pub fn elctrophoresis_voltage(&self) -> Option<i32> {
        self.get_i32("EPVt", 1)
    }

    // Downsampling factor
    pub fn downsampling_factor(&self) -> Option<i32> {
        self.get_i32("DSam", 1)
    }

    // Detector Cell Heater Temperature
    pub fn detector_cell_heater_temp(&self) -> Option<i32> {
        self.get_i32("DCHT", 1)
    }

    // Is Capillary Machine?
    pub fn is_capillary_machine(&self) -> Option<u8> {
        self.get_u8("CpEP", 1)
    }

    // Comment about sample (optional)
    pub fn sample_comment(&self) -> Option<String> {
        self.get_text("CMNT", 1)
    }
}


#[derive(Debug, Serialize, Deserialize)]
pub struct Metadata {
    pub analysis_protocol: AnalysisProtocol,
    pub software_versions: SoftwareVersions,
    pub peak_spacing: PeakSpacing,
    pub separation_medium: SeparationMedium,
    pub run: Run,
    pub data_collection: DataCollection,
    pub results_group: ResultsGroup,
    pub plate: Plate,
    pub instrument: Instrument,
    pub dyes: Dyes,
    pub container: Container,
    pub analysis: Analysis,

    pub scans: Option<i32>,
    pub sample_name: Option<String>,
    pub dye_signal_strength: Option<(i32, i32, i32, i32)>,
    pub complemented: Option<i32>,
    pub pixel_bin_size: Option<i32>,
    pub baseline_noise: Option<(f32, f32, f32, f32)>,
    pub total_capillaries: Option<i32>,
    pub averaged_columns: Option<i32>,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct TraceData {
    pub peak_locations: PeakLocations,
    pub quality_values: QualityValues,
    pub sequences: SequenceValues,
    pub raw_colors: RawColorData,
    pub analyzed_colors: AnalyzedColorData,
    pub measurements: MeasurementData,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct AnalysisProtocol {
    pub settings_name: Option<String>,
    pub settings_version: Option<String>,
    pub xml: Option<String>,
    pub xml_schema_version: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SoftwareVersions {
    pub data_collection: Option<String>,
    pub sizecaller: Option<String>,
    pub firmware: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PeakSpacing {
    pub avg_used_in_last_analysis: Option<f32>,
    pub avg_last_calculated: Option<f32>,
    pub basecaller_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SeparationMedium {
    pub lot_number: Option<String>,
    pub lot_expiry: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Run {
    pub name: Option<String>,
    pub temperature: Option<i32>,
    pub start_date: Option<String>,
    pub start_time: Option<String>,
    pub end_date: Option<String>,
    pub end_time: Option<String>,

    pub protocol_name: Option<String>,
    pub protocol_version: Option<String>,

    pub module_name: Option<String>,
    pub module_version: Option<String>,
    pub module_schema_version: Option<String>,
    pub module_xml: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DataCollection {
    pub start_date: Option<String>,
    pub start_time: Option<String>,
    pub end_date: Option<String>,
    pub end_time: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ResultsGroup {
    pub name: Option<String>,
    pub owner: Option<String>,
    pub comment: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Plate {
    pub r#type: Option<String>,
    pub size: Option<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Instrument {
    /// Instrument Class
    pub class: Option<String>,
    /// Instrument Family
    pub family: Option<String>,
    /// Official Instrument Name
    pub name: Option<String>,
    /// Instrument Parameters
    pub parameters: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Dyes {
    /// Number of dyes
    pub total: Option<i32>,
    /// Dye set name
    pub name: Option<String>,

    pub dye1: Option<Dye>,
    pub dye2: Option<Dye>,
    pub dye3: Option<Dye>,
    pub dye4: Option<Dye>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Dye {
    pub name: Option<String>,
    pub wavelength: Option<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Container {
    // Container identifier (plate barcode, for example)
    pub id: Option<String>,
    // Container name (usually identical to CTID)
    pub name: Option<String>,
    // Container owner
    pub owner: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Analysis {
    /// Analysis Return code. Produced only by 5 Prime basecaller 1.0b3
    pub return_code: Option<i32>,
    /// Flag to indicate whether adaptive processing worked or not
    pub adaptive_processing: Option<i32>,
    /// Analysis Starting scan number for first analysis
    pub starting_scan_first: Option<i32>,
    /// Analysis Starting scan number for last analysis
    pub starting_scan_last: Option<i32>,
    /// Analysis Ending scan number for basecalling on initial analysis
    pub ending_scan_first: Option<i32>,
    /// Analysis Ending scan number for basecalling on last analysis
    pub ending_scan_last: Option<i32>,
    /// Reference scan number for mobility and spacing curves for first analysis
    pub ref_scan_first: Option<i32>,
    /// Reference scan number for mobility and spacing curves for last analysis
    pub ref_scan_last: Option<i32>,
    /// Basecaller timestamp. Time of completion of most recent analysis
    pub basecaller_completed: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PeakLocations {
    /// Array of peak locations edited by user
    pub edited_by_user: Option<Vec<i32>>,
    /// Array of peak locations as called by Basecaller
    pub called_by_basecaller: Option<Vec<i32>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct QualityValues {
    /// Array of quality Values (0-255) as edited by user
    pub edited_by_user: Option<Vec<u8>>,
    /// Array of quality values (0-255) as called by Basecaller
    pub called_by_basecaller: Option<Vec<u8>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SequenceValues {
    /// Array of sequence characters edited by user
    pub edited_by_user: Option<Vec<u8>>,
    /// Array of sequence characters as called by Basecaller
    pub called_by_basecaller: Option<Vec<u8>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AnalyzedColorData {
    /// DATA-9 Short Array holding analyzed color data
    pub channel_1: Option<Vec<i32>>,
    /// DATA-10 Short Array holding analyzed color data
    pub channel_2: Option<Vec<i32>>,
    /// DATA-11 Short Array holding analyzed color data
    pub channel_3: Option<Vec<i32>>,
    /// DATA-12 Short Array holding analyzed color data
    pub channel_4: Option<Vec<i32>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MeasurementData {
    /// Voltage, measured (decavolts)
    pub voltage: Option<Vec<i32>>,
    /// Current, measured (milliAmps)
    pub current: Option<Vec<i32>>,
    /// Power, measured (milliWatts)
    pub power: Option<Vec<i32>>,
    /// Temperature, measured (degrees C)
    pub temperature: Option<Vec<i32>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RawColorData {
    /// Channel 1 raw data
    pub channel_1: Option<Vec<i32>>,
    /// Channel 2 raw data
    pub channel_2: Option<Vec<i32>>,
    /// Channel 3 raw data
    pub channel_3: Option<Vec<i32>>,
    /// Channel 4 raw data
    pub channel_4: Option<Vec<i32>>,
}



fn chars_to_utf8(chars: &Vec<u8>) -> String {
    String::from_utf8_lossy(&chars).to_string()
}
