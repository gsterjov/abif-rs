use std::io::{self, Read, Seek, SeekFrom};

use crate::{record, header, Error};
use record::Record;
use header::{Header, DirectoryEntry};
use super::MAGIC_NUMBER;


pub struct Reader<R> {
    inner: R,
}

pub type Directory = Vec<DirectoryEntry>;
pub type Records = Vec<Record>;

impl<R> Reader<R>
where R: Read + Seek,
{
    pub fn new(inner: R) -> Self {
        Self { inner }
    }

    pub fn read_header(&mut self) -> Result<Header, Error> {
        read_magic(&mut self.inner)?;
        header::read_header(&mut self.inner)
    }

    pub fn read_directory(&mut self, header: &Header) -> Result<Directory, Error> {
        read_directory(&mut self.inner, header)
    }

    pub fn read_records(&mut self, directory: &Directory) -> Result<Records, Error> {
        read_records(&mut self.inner, directory)
    }
}

fn read_magic<R>(reader: &mut R) -> io::Result<()>
where R: Read,
{
    let mut magic = [0; 4];
    reader.read_exact(&mut magic)?;

    if magic == MAGIC_NUMBER {
        Ok(())
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "invalid ABIF header",
        ))
    }
}

fn read_directory<R>(reader: &mut R, header: &Header) -> Result<Directory, Error>
where R: Read + Seek,
{
    reader.seek(SeekFrom::Start(header.data_offset as u64))?;

    let mut directory = Directory::with_capacity(header.entries as usize);
    for _ in 1..header.entries {
        directory.push(header::read_entry(reader)?);
    }

    Ok(directory)
}

fn read_records<R>(reader: &mut R, directory: &Directory) -> Result<Records, Error>
where R: Read + Seek,
{
    let mut records = Records::with_capacity(directory.len());
    for entry in directory {
        let record = record::read_record(reader, entry)?;
        records.push(record);
    }

    Ok(records)
}
