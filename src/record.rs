use std::{io::{self, Read, Seek, SeekFrom}, ffi::CStr};

use super::Error;
use crate::header::{DirectoryEntry, ElementType, Tag};

#[derive(Debug)]
pub enum RecordData {
    Text(String),
    Integers(Vec<i32>),
    Floats(Vec<f32>),
    Doubles(Vec<f64>),
    Bytes(Vec<u8>),
    Booleans(Vec<bool>),
    Unsupported,
}

#[derive(Debug)]
pub struct Record {
    pub tag: Tag,
    pub data: RecordData,
}

impl Record {
    fn new(tag: Tag, data: RecordData) -> Record {
        Record { tag, data }
    }
}


pub fn read_record<R>(reader: &mut R, entry: &DirectoryEntry) -> Result<Record, Error>
where R: Read + Seek,
{
    let data = get_data(reader, entry)?;
    let tag = entry.tag.clone();

    let record = match entry.element_type {
        ElementType::Byte => Record::new(tag, RecordData::Bytes(data)),
        ElementType::Char => Record::new(tag, RecordData::Bytes(data)),
        ElementType::Word => Record::new(tag, RecordData::Integers(read_word(&data)?)),
        ElementType::Short => Record::new(tag, RecordData::Integers(read_short(&data)?)),
        ElementType::Long => Record::new(tag, RecordData::Integers(read_long(&data)?)),
        ElementType::Float => Record::new(tag, RecordData::Floats(read_float(&data)?)),
        ElementType::Double => Record::new(tag, RecordData::Doubles(read_double(&data)?)),
        ElementType::Date => Record::new(tag, RecordData::Text(read_date(&data)?)),
        ElementType::Time => Record::new(tag, RecordData::Text(read_time(&data)?)),
        ElementType::PString => Record::new(tag, RecordData::Text(read_pstring(&data)?)),
        ElementType::CString => Record::new(tag, RecordData::Text(read_cstring(&data)?)),
        ElementType::Thumbprint => Record::new(tag, RecordData::Bytes(data)),
        ElementType::Bool => Record::new(tag, RecordData::Booleans(read_bool(&data)?)),
        ElementType::UserData => Record::new(tag, RecordData::Bytes(data)),
        ElementType::Unsupported => Record::new(tag, RecordData::Unsupported),
    };

    Ok(record)
}


fn get_data<R>(reader: &mut R, entry: &DirectoryEntry) -> Result<Vec<u8>, Error>
where R: Read + Seek,
{
    let size = (entry.element_size as usize) * (entry.num_elements as usize);
    let data = if size > 4 {
        let mut data = vec![0; size];
        reader.seek(SeekFrom::Start(entry.data_offset as u64))?;
        reader.read_exact(&mut data)?;
        data
    } else {
        let bytes = entry.data_offset.to_be_bytes();
        bytes[..size].to_vec()
    };

    Ok(data)
}

fn read_long(data: &[u8]) -> Result<Vec<i32>, Error> {
    let mut arr = Vec::new();

    for chunk in data.chunks_exact(4) {
        let element : [u8; 4] = chunk.try_into().unwrap();
        arr.push(i32::from_be_bytes(element));
    }

    Ok(arr)
}

fn read_date(data: &[u8]) -> Result<String, Error> {
    let year = i16::from_be_bytes([data[0], data[1]]);
    Ok(format!("{:04}-{:02}-{:02}", year, data[2], data[3]))
}

fn read_time(data: &[u8]) -> io::Result<String> {
    Ok(format!("{:02}:{:02}:{:02}.{:02}", data[0], data[1], data[2], data[3]))
}

fn read_short(data: &[u8]) -> Result<Vec<i32>, Error> {
    let mut arr = Vec::new();

    for chunk in data.chunks_exact(2) {
        let element : [u8; 2] = chunk.try_into().unwrap();
        arr.push(i16::from_be_bytes(element) as i32);
    }

    Ok(arr)
}

fn read_word(data: &[u8]) -> Result<Vec<i32>, Error> {
    let mut arr = Vec::new();

    for chunk in data.chunks_exact(2) {
        let element : [u8; 2] = chunk.try_into().unwrap();
        arr.push(u16::from_be_bytes(element) as i32);
    }

    Ok(arr)
}

fn read_float(data: &[u8]) -> Result<Vec<f32>, Error> {
    let mut arr = Vec::new();

    for chunk in data.chunks_exact(4) {
        let element : [u8; 4] = chunk.try_into().unwrap();
        arr.push(f32::from_be_bytes(element));
    }

    Ok(arr)
}

fn read_double(data: &[u8]) -> Result<Vec<f64>, Error> {
    let mut arr = Vec::new();

    for chunk in data.chunks_exact(8) {
        let element : [u8; 8] = chunk.try_into().unwrap();
        arr.push(f64::from_be_bytes(element));
    }

    Ok(arr)
}

fn read_bool(data: &[u8]) -> Result<Vec<bool>, Error> {
    let mut arr = Vec::new();
    for byte in data {
        arr.push(*byte != 0u8);
    }
    Ok(arr)
}

fn read_cstring(data: &[u8]) -> Result<String, Error> {
    match CStr::from_bytes_with_nul(data) {
        Ok(c_str) => {
            Ok(c_str.to_string_lossy().to_string())
        }
        Err(e) => Err(Error::InvalidData(e.to_string()))
    }
}

fn read_pstring(data: &[u8]) -> Result<String, Error> {
    let char_count = data[0] as usize;
    if char_count != (data.len() - 1) {
        let message = String::from("Pascal string character count not equal to element count");
        return Err(Error::InvalidData(message));
    }

    let value = String::from_utf8_lossy(&data[1..]).to_string();
    Ok(value)
}
