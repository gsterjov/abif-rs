use std::io::Read;
use byteorder::{BigEndian, ReadBytesExt};

use crate::Error;


#[derive(Debug)]
pub struct Header {
    pub version: u16,
    pub entries: i32,
    pub data_size: i32,
    pub data_offset: i32,
}

#[derive(Debug)]
pub struct DirectoryEntry {
    pub tag: Tag,
    pub element_type: ElementType,
    pub element_size: i16,
    pub num_elements: i32,
    pub data_size: i32,
    pub data_offset: i32,
    pub data_handle: i32,
}

#[derive(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Tag {
    pub name: String,
    pub number: i32,
}

impl std::fmt::Display for Tag {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}-{}", self.name, self.number)
    }
}

impl Tag {
    pub fn new(name: String, number: i32) -> Tag {
        Tag { name, number }
    }
}

#[derive(Debug)]
pub enum ElementType {
    // supported types
    Byte,
    Char,
    Word,
    Short,
    Long,
    Float,
    Double,
    Date,
    Time,
    PString,
    CString,

    // legaacy types
    Thumbprint,
    Bool,
    UserData,

    // unsupported legacy types
    Unsupported,
}

impl From<i16> for ElementType {
    fn from(source: i16) -> Self {
        match source {
            1 => Self::Byte,
            2 => Self::Char,
            3 => Self::Word,
            4 => Self::Short,
            5 => Self::Long,
            7 => Self::Float,
            8 => Self::Double,
            10 => Self::Date,
            11 => Self::Time,
            12 => Self::Thumbprint,
            13 => Self::Bool,
            18 => Self::PString,
            19 => Self::CString,
            _ if source >= 1024 => Self::UserData,
            _ => Self::Unsupported,
        }
    }
}

pub fn read_header<R>(reader: &mut R) -> Result<Header, Error>
where R: Read,
{
    let version = reader.read_u16::<BigEndian>()?;
    let entry = read_entry(reader)?;

    Ok(Header {
        version,
        entries: entry.num_elements,
        data_size: entry.data_size,
        data_offset: entry.data_offset,
    })
}

pub fn read_entry<R>(reader: &mut R) -> Result<DirectoryEntry, Error>
where R: Read,
{
    let mut name = [0; 4];
    reader.read_exact(&mut name)?;
    let name = String::from_utf8_lossy(&name).to_string();
    let number = reader.read_i32::<BigEndian>()?;

    Ok(DirectoryEntry {
        tag: Tag::new(name, number),
        element_type: ElementType::from(reader.read_i16::<BigEndian>()?),
        element_size: reader.read_i16::<BigEndian>()?,
        num_elements: reader.read_i32::<BigEndian>()?,
        data_size: reader.read_i32::<BigEndian>()?,
        data_offset: reader.read_i32::<BigEndian>()?,
        data_handle: reader.read_i32::<BigEndian>()?,
    })
}
