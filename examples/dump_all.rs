//! Dumps all records found in an ABIF file.

use std::env;
use std::fs::File;

use abif;

fn main() -> Result<(), abif::Error> {
    let src = env::args().nth(1).expect("missing src");

    let file = File::open(src)?;
    let mut reader = abif::Reader::new(file);

    let header = reader.read_header()?;
    let directory = reader.read_directory(&header)?;
    let records = reader.read_records(&directory)?;

    for record in records {
        println!("{}: {:?}", record.tag, record.data);
    }

    Ok(())
}
