//! Dumps the raw and analyzed color data of the trace.

use std::env;
use std::fs::File;

use abif::Abif;

fn main() -> Result<(), abif::Error> {
    let src = env::args().nth(1).expect("missing src");

    let file = File::open(src)?;
    let abif = Abif::read(file)?;

    let data = serde_json::to_string(&abif.trace_data()).unwrap();
    println!("{data}");

    Ok(())
}
