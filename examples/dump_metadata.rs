//! Dumps all the metadata in an ABIF file.

use std::env;
use std::fs::File;

use abif::Abif;

fn main() -> Result<(), abif::Error> {
    let src = env::args().nth(1).expect("missing src");

    let file = File::open(src)?;
    let abif = Abif::read(file)?;

    let metadata = serde_json::to_string_pretty(&abif.metadata()).unwrap();
    println!("{metadata}");

    Ok(())
}
