//! Creates an SVG line graph from the trace data

use std::env;
use std::fs::File;

use hypermelon::prelude::*;
use poloto::build;

use abif::Abif;

fn main() -> Result<(), abif::Error> {
    let src = env::args().nth(1).expect("missing src");

    let file = File::open(src)?;
    let abif = Abif::read(file)?;


    let svg = poloto::header().with_viewbox_width(1200.0);

    let opt = poloto::render::render_opt()
        .with_tick_lines([true, true])
        .with_viewbox(svg.get_viewbox())
        .move_into();

    let style = poloto::render::Theme::dark()
        .append(".poloto_line{stroke-dasharray:2;stroke-width:2;}");

    let g = abif.g_analyzed().unwrap().into_iter().map(|y| y as f64);
    let a = abif.a_analyzed().unwrap().into_iter().map(|y| y as f64);
    let c = abif.c_analyzed().unwrap().into_iter().map(|y| y as f64);
    let t = abif.t_analyzed().unwrap().into_iter().map(|y| y as f64);

    let plots = poloto::plots!(
        build::plot("G").line((0..).take(2000).zip(g)),
        build::plot("A").line((0..).take(2000).zip(a)),
        build::plot("C").line((0..).take(2000).zip(c)),
        build::plot("T").line((0..).take(2000).zip(t))
    );

    poloto::data(plots)
        .map_opt(|_| opt)
        .build_and_label(("trace", "x", "y"))
        .append_to(svg.append(style))
        .render_stdout();

    Ok(())
}
