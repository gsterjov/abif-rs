//! Counts the number of traces in an ABIF file.

use std::env;
use std::fs::File;

use abif::Abif;

fn main() -> Result<(), abif::Error> {
    let src = env::args().nth(1).expect("missing src");

    let file = File::open(src)?;
    let abif = Abif::read(file)?;

    let g = if let Some(data) = abif.g_analyzed() {
        data.len()
    } else { 0 };

    let a = if let Some(data) = abif.a_analyzed() {
        data.len()
    } else { 0 };

    let t = if let Some(data) = abif.t_analyzed() {
        data.len()
    } else { 0 };

    let c = if let Some(data) = abif.c_analyzed() {
        data.len()
    } else { 0 };

    println!("{:?}", abif.base_order());
    println!("G: {g}");
    println!("A: {a}");
    println!("T: {t}");
    println!("C: {c}");

    Ok(())
}
